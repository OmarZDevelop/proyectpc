﻿using BusinessLogic;
using DataAccessModel.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace WebApiPicoPlaca.Controllers
{
    /*******************************************************
     Clase Controllador del WebApi esta clase extiende de ApiController
     y en esta se crea los metodos que van a ser expuestos como REST en la nube
     *******************************************************/


    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PcRuleController: ApiController
    {
        RuleLogic logic = new RuleLogic();

        //Metodo Get para consultar si un carro puede circular segun criterios de busqueda
        [HttpGet]
        public Response isCarEnable(string num_plate,string date,string time)
        {
            return logic.chekCarEnable(num_plate, date, time);
        }
    }
}