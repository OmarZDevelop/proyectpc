USE [master]
GO
/****** Object:  Database [PicoPlacaDB]    Script Date: 25/1/2020 11:49:08 ******/
CREATE DATABASE [PicoPlacaDB] ON  PRIMARY 
( NAME = N'PicoPlacaDB', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL10.SQLEXPRESS\MSSQL\DATA\PicoPlacaDB.mdf' , SIZE = 2048KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'PicoPlacaDB_log', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL10.SQLEXPRESS\MSSQL\DATA\PicoPlacaDB_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [PicoPlacaDB] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PicoPlacaDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PicoPlacaDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PicoPlacaDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PicoPlacaDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PicoPlacaDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PicoPlacaDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [PicoPlacaDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [PicoPlacaDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PicoPlacaDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PicoPlacaDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PicoPlacaDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PicoPlacaDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PicoPlacaDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PicoPlacaDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PicoPlacaDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PicoPlacaDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [PicoPlacaDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PicoPlacaDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PicoPlacaDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PicoPlacaDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PicoPlacaDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PicoPlacaDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PicoPlacaDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PicoPlacaDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [PicoPlacaDB] SET  MULTI_USER 
GO
ALTER DATABASE [PicoPlacaDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PicoPlacaDB] SET DB_CHAINING OFF 
GO
USE [PicoPlacaDB]
GO
/****** Object:  Table [dbo].[pc_day]    Script Date: 25/1/2020 11:49:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pc_day](
	[id_day] [int] IDENTITY(1,1) NOT NULL,
	[txt_description] [varchar](50) NOT NULL,
 CONSTRAINT [PK_pc_day] PRIMARY KEY CLUSTERED 
(
	[id_day] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pc_location_restriction_catalog]    Script Date: 25/1/2020 11:49:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pc_location_restriction_catalog](
	[id_location] [int] IDENTITY(1,1) NOT NULL,
	[txt_description] [varchar](50) NOT NULL,
 CONSTRAINT [PK_pc_location_restriction_catalog] PRIMARY KEY CLUSTERED 
(
	[id_location] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pc_period]    Script Date: 25/1/2020 11:49:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pc_period](
	[id_period] [int] IDENTITY(1,1) NOT NULL,
	[txt_description] [varchar](50) NOT NULL,
 CONSTRAINT [pK_pc_period] PRIMARY KEY CLUSTERED 
(
	[id_period] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pc_rule]    Script Date: 25/1/2020 11:49:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pc_rule](
	[id_rule] [int] IDENTITY(1,1) NOT NULL,
	[id_day] [int] NOT NULL,
	[start_time] [time](7) NOT NULL,
	[end_time] [time](7) NOT NULL,
	[id_period] [int] NOT NULL,
 CONSTRAINT [PK_pc_rule] PRIMARY KEY CLUSTERED 
(
	[id_rule] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pc_validator_digit]    Script Date: 25/1/2020 11:49:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pc_validator_digit](
	[id_validator_digit] [int] IDENTITY(1,1) NOT NULL,
	[digit_value] [int] NOT NULL,
	[id_rule] [int] NOT NULL
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[pc_day] ON 

INSERT [dbo].[pc_day] ([id_day], [txt_description]) VALUES (1, N'LUNES')
INSERT [dbo].[pc_day] ([id_day], [txt_description]) VALUES (2, N'MARTES')
INSERT [dbo].[pc_day] ([id_day], [txt_description]) VALUES (3, N'MIERCOLES')
INSERT [dbo].[pc_day] ([id_day], [txt_description]) VALUES (4, N'JUEVES')
INSERT [dbo].[pc_day] ([id_day], [txt_description]) VALUES (5, N'VIERNES')
INSERT [dbo].[pc_day] ([id_day], [txt_description]) VALUES (6, N'SABADO')
INSERT [dbo].[pc_day] ([id_day], [txt_description]) VALUES (7, N'DOMINGO')
SET IDENTITY_INSERT [dbo].[pc_day] OFF
SET IDENTITY_INSERT [dbo].[pc_period] ON 

INSERT [dbo].[pc_period] ([id_period], [txt_description]) VALUES (1, N'MAÑANA')
INSERT [dbo].[pc_period] ([id_period], [txt_description]) VALUES (2, N'TARDE')
SET IDENTITY_INSERT [dbo].[pc_period] OFF
SET IDENTITY_INSERT [dbo].[pc_rule] ON 

INSERT [dbo].[pc_rule] ([id_rule], [id_day], [start_time], [end_time], [id_period]) VALUES (1, 1, CAST(N'07:00:00' AS Time), CAST(N'09:30:00' AS Time), 1)
INSERT [dbo].[pc_rule] ([id_rule], [id_day], [start_time], [end_time], [id_period]) VALUES (2, 1, CAST(N'16:00:00' AS Time), CAST(N'19:30:00' AS Time), 2)
INSERT [dbo].[pc_rule] ([id_rule], [id_day], [start_time], [end_time], [id_period]) VALUES (3, 2, CAST(N'07:00:00' AS Time), CAST(N'09:30:00' AS Time), 1)
INSERT [dbo].[pc_rule] ([id_rule], [id_day], [start_time], [end_time], [id_period]) VALUES (4, 2, CAST(N'16:00:00' AS Time), CAST(N'19:30:00' AS Time), 2)
INSERT [dbo].[pc_rule] ([id_rule], [id_day], [start_time], [end_time], [id_period]) VALUES (5, 3, CAST(N'07:00:00' AS Time), CAST(N'09:30:00' AS Time), 1)
INSERT [dbo].[pc_rule] ([id_rule], [id_day], [start_time], [end_time], [id_period]) VALUES (6, 3, CAST(N'16:00:00' AS Time), CAST(N'19:30:00' AS Time), 2)
INSERT [dbo].[pc_rule] ([id_rule], [id_day], [start_time], [end_time], [id_period]) VALUES (7, 4, CAST(N'07:00:00' AS Time), CAST(N'09:30:00' AS Time), 1)
INSERT [dbo].[pc_rule] ([id_rule], [id_day], [start_time], [end_time], [id_period]) VALUES (8, 4, CAST(N'16:00:00' AS Time), CAST(N'19:30:00' AS Time), 2)
INSERT [dbo].[pc_rule] ([id_rule], [id_day], [start_time], [end_time], [id_period]) VALUES (9, 5, CAST(N'07:00:00' AS Time), CAST(N'09:30:00' AS Time), 1)
INSERT [dbo].[pc_rule] ([id_rule], [id_day], [start_time], [end_time], [id_period]) VALUES (10, 5, CAST(N'16:00:00' AS Time), CAST(N'19:30:00' AS Time), 2)
SET IDENTITY_INSERT [dbo].[pc_rule] OFF
SET IDENTITY_INSERT [dbo].[pc_validator_digit] ON 

INSERT [dbo].[pc_validator_digit] ([id_validator_digit], [digit_value], [id_rule]) VALUES (1, 1, 1)
INSERT [dbo].[pc_validator_digit] ([id_validator_digit], [digit_value], [id_rule]) VALUES (2, 1, 2)
INSERT [dbo].[pc_validator_digit] ([id_validator_digit], [digit_value], [id_rule]) VALUES (3, 2, 1)
INSERT [dbo].[pc_validator_digit] ([id_validator_digit], [digit_value], [id_rule]) VALUES (4, 2, 2)
INSERT [dbo].[pc_validator_digit] ([id_validator_digit], [digit_value], [id_rule]) VALUES (5, 3, 3)
INSERT [dbo].[pc_validator_digit] ([id_validator_digit], [digit_value], [id_rule]) VALUES (6, 3, 4)
INSERT [dbo].[pc_validator_digit] ([id_validator_digit], [digit_value], [id_rule]) VALUES (7, 4, 3)
INSERT [dbo].[pc_validator_digit] ([id_validator_digit], [digit_value], [id_rule]) VALUES (8, 4, 4)
INSERT [dbo].[pc_validator_digit] ([id_validator_digit], [digit_value], [id_rule]) VALUES (9, 5, 5)
INSERT [dbo].[pc_validator_digit] ([id_validator_digit], [digit_value], [id_rule]) VALUES (10, 5, 6)
INSERT [dbo].[pc_validator_digit] ([id_validator_digit], [digit_value], [id_rule]) VALUES (11, 6, 5)
INSERT [dbo].[pc_validator_digit] ([id_validator_digit], [digit_value], [id_rule]) VALUES (12, 6, 6)
INSERT [dbo].[pc_validator_digit] ([id_validator_digit], [digit_value], [id_rule]) VALUES (13, 7, 7)
INSERT [dbo].[pc_validator_digit] ([id_validator_digit], [digit_value], [id_rule]) VALUES (14, 7, 8)
INSERT [dbo].[pc_validator_digit] ([id_validator_digit], [digit_value], [id_rule]) VALUES (15, 8, 7)
INSERT [dbo].[pc_validator_digit] ([id_validator_digit], [digit_value], [id_rule]) VALUES (16, 8, 8)
INSERT [dbo].[pc_validator_digit] ([id_validator_digit], [digit_value], [id_rule]) VALUES (17, 9, 9)
INSERT [dbo].[pc_validator_digit] ([id_validator_digit], [digit_value], [id_rule]) VALUES (18, 9, 10)
INSERT [dbo].[pc_validator_digit] ([id_validator_digit], [digit_value], [id_rule]) VALUES (19, 0, 9)
INSERT [dbo].[pc_validator_digit] ([id_validator_digit], [digit_value], [id_rule]) VALUES (20, 0, 10)
SET IDENTITY_INSERT [dbo].[pc_validator_digit] OFF
ALTER TABLE [dbo].[pc_rule]  WITH CHECK ADD  CONSTRAINT [FK_pc_rule_pc_day] FOREIGN KEY([id_day])
REFERENCES [dbo].[pc_day] ([id_day])
GO
ALTER TABLE [dbo].[pc_rule] CHECK CONSTRAINT [FK_pc_rule_pc_day]
GO
ALTER TABLE [dbo].[pc_rule]  WITH CHECK ADD  CONSTRAINT [FK_pc_rule_pc_period] FOREIGN KEY([id_period])
REFERENCES [dbo].[pc_period] ([id_period])
GO
ALTER TABLE [dbo].[pc_rule] CHECK CONSTRAINT [FK_pc_rule_pc_period]
GO
ALTER TABLE [dbo].[pc_validator_digit]  WITH CHECK ADD  CONSTRAINT [FK_validator_digit_schelude] FOREIGN KEY([id_rule])
REFERENCES [dbo].[pc_rule] ([id_rule])
GO
ALTER TABLE [dbo].[pc_validator_digit] CHECK CONSTRAINT [FK_validator_digit_schelude]
GO
USE [master]
GO
ALTER DATABASE [PicoPlacaDB] SET  READ_WRITE 
GO
