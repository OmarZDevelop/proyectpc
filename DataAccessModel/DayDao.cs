﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessModel
{
    public class DayDao
    {
        /************************************************************************
         Primero se instancia el modelo de datos mapeado a la base SQL con ADONET
         Los modelos de base de datos las crea automaticamente EntityFramework
         Luego se crean los metodos de consulta usando la semantica de Lynq
        *************************************************************************/
        PicoPlacaDBEntities db = new PicoPlacaDBEntities();
        //metodo para obtner un objeto Dia segun su nombre
        public pc_day getDayByDescription(string description)
        {
            pc_day day = new pc_day();
            try
            {
                day = db.pc_day.Where(d => d.txt_description.ToUpper() == description.ToUpper()).FirstOrDefault();
            }
            catch(Exception ex) { }
            return day;
        }
    }
}
