﻿using DataAccessModel.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessModel
{
    public class RuleDao
    {
        /************************************************************************
         Primero se instancia el modelo de datos mapeado a la base SQL con ADONET
         Los modelos de base de datos las crea automaticamente EntityFramework
         Luego se crean los metodos de consulta usando la semantica de Lynq
        *************************************************************************/
        PicoPlacaDBEntities db = new PicoPlacaDBEntities();
        
        //Consulta reglas por dia y hora
        public pc_rule getRuleByDayTime(pc_day day, TimeSpan time)
        {
            pc_rule rule_aux = new pc_rule();
            try
            {                
                rule_aux = db.pc_rule.Where(r => r.id_day == day.id_day && time>=r.start_time  && time <= r.end_time).FirstOrDefault();
            }
            catch { }
            return rule_aux;
        }
        //consulta todos los registros de la tabla digito validador por digito
        //Se realiza esta consulta para poder trer todas las reglas relacionadas a un digito verificador
        public List<pc_validator_digit> getRegisterListByDigit(int num_validator)
        {
            List<pc_validator_digit> list = new List<pc_validator_digit>();
            try
            {
                list= db.pc_validator_digit.Where(d => d.digit_value == num_validator).ToList();
                if (list.Count>0)
                {
                    foreach (pc_validator_digit item in list)
                    {
                        item.pc_rule = getRuleById(item.id_rule);
                    }
                }
            }
            catch { }
            return list;
        }
        //Se traer una regla por el ID
        public pc_rule getRuleById(int id)
        {
            pc_rule rule = new pc_rule();
            try
            {
                rule = db.pc_rule.Where(r => r.id_rule == id).FirstOrDefault();
            }
            catch { }
            return rule;
        }

    }
}
