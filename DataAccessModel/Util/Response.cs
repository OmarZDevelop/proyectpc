﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessModel.Util
{
    /***********************************************************
     Clase auxiliar creada para presentar resultados al usuario final
     en el cual se utilia una descriocion que puede ser el mensaje dirigido al usuario
     una varaible bool que indica si ocurrio algun error
     y un objeto generico si es necesario traer un objeto para ser manipulado en la vista
     **********************************************************/
    public class Response
    {
        public string description { get; set; }
        public bool error { get; set; }
        public object return_object { get; set; }
    }
}
