﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using DataAccessModel.Util;
using BusinessLogic;

namespace UnitTest
{
    /******************************************************************
     Clase creada para hacer pruebas Unitarias
    ******************************************************************/

    [TestClass]
    public class UnitTestWebApi
    {
        //Metodo para probar respuesta del metodo para valores especificos
        [TestMethod]
        public void TestMethodCheckCarEnable()
        {
            string date = "24/01/2020";
            string time = "10:00";
            string num_plate = "PHT-129";
            RuleLogic logic_test = new RuleLogic();
            Response expected_result = new Response();
            expected_result.description = "El carro  puede circular en la hora enviada";
            expected_result.error = false;
            Response result = new Response();
            result = logic_test.chekCarEnable(num_plate,date,time);

            Assert.AreEqual(expected_result,result);
        }
    }
}
