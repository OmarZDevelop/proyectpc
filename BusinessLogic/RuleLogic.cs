﻿using DataAccessModel;
using DataAccessModel.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class RuleLogic
    {
        /*************************************************************
         Clase creada para realizar toda la logica de negocio con respecto a las reglas
         del Pico y Placa
         ***********************************************************/

        RuleDao rule_dao = new RuleDao();
        DayLogic day_logic = new DayLogic();
        //Metodo para validar si un carro puede circular 
        public Response chekCarEnable(string num_plate,string date , string time_string)
        {
            Response res = new Response();
            try
            {
                //Primero se toma el ultimo digito de la placa enviada
                char last_digit_char = num_plate[num_plate.Length - 1];
                int num_validator = Convert.ToInt32(last_digit_char.ToString());
                //Segundo se convierte el string de fecha a un tipo DateTime para tomar el dia
                pc_day day = day_logic.getDayByDate(date);
                if (day==null)
                {
                    res.description = "No existe registros de días en nuesto sistema";
                    res.error = false;
                    return res;
                }
                //tercero se obtiene las reglas según el digito válidador
                List<pc_validator_digit> list_num_validator = new List<pc_validator_digit>();
                list_num_validator = rule_dao.getRegisterListByDigit(num_validator);
                if (list_num_validator.Count == 0)
                {
                    res.description = "No existe una regla para el ultimo digito de la placa consultada,";
                    res.error = false;
                    return res;
                }
                //Cuarto se Intera la lista para poder determinar la regla que se va a aplicar
                foreach (pc_validator_digit item in list_num_validator)
                {
                    //Se valida si el dia enviado esta en el rango de dias de la regla segun el digito
                    if (item.pc_rule.id_day == day.id_day)
                    {
                        //Se valida si la hora se encuetra en el rango 
                        TimeSpan time = TimeSpan.Parse(time_string);
                        pc_rule rule_aux = new pc_rule();
                        rule_aux = rule_dao.getRuleByDayTime(day, time);
                        if (rule_aux != null)
                        {
                            res.description = "El carro no puede circular en la hora enviada";
                            res.error = false;
                            res.return_object =  rule_aux;
                            return res;
                        }
                        else
                        {
                            res.description = "El carro  puede circular en la hora enviada";
                            res.error = false;
                            return res;
                        }
                    }
                }
                //Quinto si no se aplico ninguna regla quiere decir que el dia enviado no esta en el rango de dias asignados
                res.description = "El carro puede circular, en el dia enviado";
                res.error = false;
                return res;
            }
            catch (Exception ex)
            {
                res.description = "Ocurrio un error en el Sistema";
                res.error = true;
                return res;
            }
            
        }

    }
}
