﻿using DataAccessModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class DayLogic
    {
        DayDao day_dao = new DayDao();
        public pc_day getDayByDate(string date)
        {
            string description_day = "";
            DateTime date_aux = Convert.ToDateTime(date);
            pc_day day = new pc_day();
            if (date_aux.DayOfWeek == DayOfWeek.Monday)
            {
                description_day = "LUNES";
            }else if (date_aux.DayOfWeek == DayOfWeek.Tuesday)
            {
                description_day = "MARTES";
            }else if (date_aux.DayOfWeek == DayOfWeek.Wednesday)
            {
                description_day = "MIERCOLES";
            }else if (date_aux.DayOfWeek == DayOfWeek.Thursday)
            {
                description_day = "JUEVES";
            }else if (date_aux.DayOfWeek == DayOfWeek.Friday)
            {
                description_day = "VIERNES";
            }else if (date_aux.DayOfWeek == DayOfWeek.Saturday)
            {
                description_day = "SABADO";
            }else if (date_aux.DayOfWeek == DayOfWeek.Sunday)
            {
                description_day = "DOMINGO";
            }
            day = day_dao.getDayByDescription(description_day);
            return day;
        }
    }
}
